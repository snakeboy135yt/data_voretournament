# Development

## New Features

- Include launcher scripts for the dedicated server and 32-bit systems.
- A dark secret awaits. Will you be the one to find it?

## Gameplay changes

- Remove the Feed and Swallow keys, use the primary and alternate fire buttons instead. When able to swallow or feed yourself to a player, the attack buttons no longer fire the weapon but act as vore keys.

# Release 2.0.1

## New features

- Include scripts for launching Vore Tournament under Windows and Linux. Mac users will still have to create a custom shortcut manually.
- Predators can now maintain their prey after being eaten, allowing players to be the prey of players who are themselves the prey of other players.
- Predators may eat dead prey on the ground. Note that this is only enabled in theory: The trace may fail due to Xonotic disabling collision boxes on dead bodies.
- Create notification icons for the vore death types.
- New scoreboard icons for indicating player colors.
- Vastly rework and improve the bot AI. Bots are now much smarter, with additional support for bots acting as willing prey.
- Bots learn which players are usually a predator or prey, and are more likely to chase or avoid based on how they perceive each player.
- Implement size based movement logic to improve navigation for micro / macro bots.
- Willing prey will refuse to allow itself to be regurgitated. This only counts for preds deliberately regurgitating their prey, it doesn't affect regurgitation induced by other factors.
- The zoom button may be used to peek outside the stomach and through the predator's mouth.
- Rework the micro / macro size calculation system, allowing multiple factors to be used: Random scale, based on health, based on armor, or based on average between health and armor.
- A new set of default bots, based on characters with specific personalities and likes / dislikes.

## Gameplay changes

- Forbid firing weapons inside a stomach by default. While allowing prey to shoot guns inside their predator is desired, there's currently no satisfactory solution for doing this properly.
- Disable prey view snapping upon grabbing by default, due to the mechanic posing an annoyance to players. Additionally allow scale difference to affect it.
- Team healing has been merged with digestion. Digesting will heal team mates instead of damaging them, while not digesting no longer has any effect.
- Prey no longer takes full damage from external projectiles, only attacks from neighboring prey.
- Don't allow players to be eaten when their spawn shield is active or they're in a vehicle. Previously we only prevented this for the act of swallowing, not the progress based check.
- Don't allow players to fire weapons when under or over a given size. This simulates micros inability to pull the trigger once the gun becomes too big, and macros being unable to press the trigger on a tiny gun.
- Don't apply health and armor limits for swallowing to team mates by default, this made team healing more problematic than it was worth.
- The speeds of swallow and digestion damage depend on the swallow progress and respectively the stomach size of the predator.
- Use 1st person death camera by default, letting prey maintain the internal view after dying.
- Remove the power mutator, it was an annoyance with no practical use.

## Bug fixes

- Fix micro / macro bounding boxes. Large players may now be too big to fit in some areas (without crouching) whereas small players can be tiny enough to enter tight spaces.
- Fix micro / macro footsteps playing when dead bodies slide across the ground.
- Fix prey being regurgitated at the respawn position of the predator instead of the last location of their death. Rework the prey addition and removal code in the process.
- Fix prey taking damage from liquids. Water and lava will no longer drown or burn prey, as long as the predator is still alive.
- Fix swallow progress remaining stuck when the prey or predator died or disconnected in some circumstances.
- Fix stomach model shaking, improve positional calculations for the vore view models.
- Fix the vore flash showing while prey was being healed.
- Fix brightness and volume changes not being undone when the menu is opened by a prey player.
- Fix dead bodies sliding across the ground when regurgitated.
- Fix internal and external view models clipping in the incorrect order.
- Implement a satisfactory way of precaching vore related player models when sv_precacheplayermodels is disabled.
- Make the Freezetag gametype compatibile with the vore system.
- Resolve incompatibility with the fly and noclip cheats.
- Don't allow prey to be grabbed using the drag cheat.
- Don't allow macros to use teleporters by default. On some maps this positions them inside solid surfaces and causes them to get stuck.
