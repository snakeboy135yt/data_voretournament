// checks whether the predator's prey currently puts them at danger of dying
// skill level 0 to 4: The bot has no awareness of danger
// skill level 5 to 6: The bot only accounts the danger posed by one prey
// skill level 7 to 10: The bot accounts the danger posed by all prey
bool Vore_CheckOverload(entity pred)
{
	if not(IS_PRED(pred) && autocvar_g_vore_kick)
		return false;
	if not(skill >= 5) // SKILL 5 and up
		return false;

	// don't count team mates and dead prey
	float kick_dmg = 0;
	FOREACH_CLIENT(IS_PLAYER(it) && it != pred && it.predator == pred && !SAME_TEAM(it, pred) && !IS_DEAD(it), {
		if(skill >= 7) // SKILL 7 and up
		{
			// account the damage of all prey
			// if macro is enabled, account for the size of prey when calculating kick damage
			// if macro is disabled, only account for pure damage when calculating kick damage
			// if this is a willing prey, only be half as paranoid that they might kick
			float will = Vore_IsWilling(it, pred) ? 0.5 : 1;
			if(pred.scale && it.scale)
				kick_dmg += autocvar_g_balance_vore_kick_damage * will * pow(it.scale / pred.scale, autocvar_g_balance_vore_kick_scalediff);
			else
				kick_dmg += autocvar_g_balance_vore_kick_damage * will;
		}
		else
		{
			// account the damage of one prey
			kick_dmg = autocvar_g_balance_vore_kick_damage;
			break;
		}
	});

	if(pred.health <= kick_dmg)
		return true;
	return false;
}

// checks if a player can be team healed
bool Vore_CheckHeal(entity prey)
{
	if(!autocvar_g_vore_teamvore || !autocvar_g_balance_vore_digestion_teamheal)
		return false;

	if(prey.health < autocvar_g_balance_vore_digestion_teamheal_health_stable)
		return true;
	if(prey.armorvalue < autocvar_g_balance_vore_digestion_teamheal_armor_stable)
		return true;

	return false;
}

// returns the difference in strength between the predator and prey
float Vore_CheckDifference(entity pred, entity prey, int health_stable, int armor_stable)
{
	float difference = 0;
	if(health_stable)
		difference += (pred.health - prey.health) / health_stable;
	if(armor_stable)
		difference += (pred.armorvalue - prey.armorvalue) / armor_stable;
	if(health_stable && armor_stable)
		difference /= 2;
	return bound(-1, difference, 1);
}

// returns the priority of a given prey against the predator
float Vore_Priority(entity pred, entity prey)
{
	// returning 0 means we want to ignore this player entirely (including firing at them)
	// returning 1 means we want to treat them as we would normally and take no decision
	// otherwise return a value which represents lower or higher interest in this player compared to others

	if(IS_DEAD(prey) || IS_PREY(prey))
		return 0; // ignore dead players and existing prey
	if(PHYS_INPUT_BUTTON_CHAT(prey))
		return 0; // ignore players who are chatting
	if(time < prey.spawnshieldtime)
		return 0; // spawn shield means the prey is invincible
	if(!autocvar_bot_ai_vore_friendly && SAME_TEAM(pred, prey) && !Vore_CheckHeal(pred) && !Vore_CheckHeal(prey))
		return 1; // pass if the pred and prey are part of the same team and we can't heal or be healed
	if(pred.flagcarried || pred.ballcarried)
		return 1; // don't risk if we are a carrier
	if(Vore_CheckOverload(pred))
		return 1; // don't hunt for more players if we're already in danger
	if(skill >= 3) // SKILL 3 and up
	if(!Vore_CanSwallow(pred, prey, false) && !Vore_CanSwallow(prey, pred, true))
		return 1; // if we're interested in swallowing or being swallowed but neither is possible, stop here
	if(skill >= 7) // SKILL 7 and up
	if(!Vore_CanEat(pred, prey) && !Vore_CanEat(prey, pred))
		return 1; // if we're interested in eating or being eaten but neither is possible, stop here

	int prey_entnum = num_for_edict(prey);
	float like_max = max(pred.bot_likepred, pred.bot_likeprey);

	// calculate our relationship with the prey
	// if the prey is a bot, also account for their prey / predator skills in how we treat them
	float relationship_value = pred.relation[prey_entnum];
	if(clienttype(prey) == CLIENTTYPE_BOT)
	{
		relationship_value = (pred.relation[prey_entnum] + (prey.bot_likeprey - prey.bot_likepred)) / 2;
		relationship_value *= autocvar_bot_ai_vore_relation_bots;
	}
	float relationship_desired = bound(-1, pred.bot_likepred - pred.bot_likeprey, 1);
	float relationship = 1 + (relationship_value * relationship_desired);

	// calculate the base priority of the target
	float priority = (prey.scale && pred.scale) ? pred.scale / prey.scale : 1; // start from the scale difference
	if(pred.stomach_fill > 0)
		priority *= 2 - pred.stomach_fill; // be twice less eager to pick a target the closer we are to our stomach capacity
	if(Vore_IsWilling(prey, pred))
		priority *= 2; // prefer willing prey twice as much
	if(IS_SWALLOWED(prey) && prey.swallower == pred)
		priority *= 1 + STAT(VORE_PROGRESS_PRED, pred); // prefer the prey we're currently swallowing based on how far we got
	if(pred.items & IT_STRENGTH)
		priority *= autocvar_g_balance_powerup_strength_damage; // the bot is more confident when they have the Strenght powerup
	if(pred.items & IT_INVINCIBLE)
		priority /= autocvar_g_balance_powerup_invincible_takedamage; // the bot is more confident when they have the Invincible powerup

	// friendly and hostile targets are evaluated based on different principles
	if(SAME_TEAM(pred, prey))
	{
		// calculate the priority of friendly targets
		float difference = Vore_CheckDifference(pred, prey, autocvar_g_balance_vore_digestion_teamheal_health_stable, autocvar_g_balance_vore_digestion_teamheal_armor_stable);

		priority *= 1 + (like_max * autocvar_bot_ai_vore_goal_friendly);
		priority *= 1 + (fabs(difference + (pred.bot_likepred - pred.bot_likeprey)) / 2);
		if(autocvar_g_balance_vore_digestion_teamheal && prey.health < autocvar_g_balance_vore_digestion_teamheal_health_stable)
			priority /= max(prey.health / autocvar_g_balance_vore_digestion_teamheal_health_stable, 0.1); // how much the team mate needs health, capped at 10x
		if(autocvar_g_balance_vore_digestion_teamheal && prey.armorvalue < autocvar_g_balance_vore_digestion_teamheal_armor_stable)
			priority /= max(prey.armorvalue / autocvar_g_balance_vore_digestion_teamheal_armor_stable, 0.1); // how much the team mate needs armor, capped at 10x
		if(autocvar_g_balance_vore_digestion_teamheal && pred.health < autocvar_g_balance_vore_digestion_teamheal_health_stable)
			priority /= max(pred.health / autocvar_g_balance_vore_digestion_teamheal_health_stable, 0.1); // how much we need health, capped at 10x
		if(autocvar_g_balance_vore_digestion_teamheal && pred.armorvalue < autocvar_g_balance_vore_digestion_teamheal_armor_stable)
			priority /= max(pred.armorvalue / autocvar_g_balance_vore_digestion_teamheal_armor_stable, 0.1); // how much we need armor, capped at 10x
		if(autocvar_bot_ai_vore_relation > 0 && autocvar_bot_ai_vore_relation_team > 0)
			priority *= pow(relationship, autocvar_bot_ai_vore_relation * autocvar_bot_ai_vore_relation_team); // evaluate relationship
		if(skill >= 3) // SKILL 3 and up
		if(IS_PRED(pred) && !Vore_HasTeamPrey(pred, pred, false))
			return 1; // we have enemy prey in our stomach, don't think about chasing team mates
		if(skill >= 5) // SKILL 5 and up
		if(prey.flagcarried || prey.ballcarried)
			return 1; // don't go after carriers and ruin their job
		if(skill >= 7) // SKILL 7 and up
		if(PHYS_INPUT_BUTTON_ATCK(prey) || PHYS_INPUT_BUTTON_ATCK2(prey))
			return 1; // a team mate wouldn't want us interfering while they're attacking or voring
	}
	else
	{
		// calculate the priority of hostile targets
		float difference = Vore_CheckDifference(pred, prey, max(autocvar_g_balance_health_regenstable, autocvar_g_balance_health_rotstable), max(autocvar_g_balance_armor_regenstable, autocvar_g_balance_armor_rotstable));

		priority *= 1 + (like_max * autocvar_bot_ai_vore_goal_hostile);
		priority *= 1 + (fabs(difference + (pred.bot_likepred - pred.bot_likeprey)) / 2);
		if(prey.items & IT_STRENGTH)
			priority /= autocvar_g_balance_powerup_strength_damage; // avoid bots that have the Strenght powerup
		if(prey.items & IT_INVINCIBLE)
			priority *= autocvar_g_balance_powerup_invincible_takedamage; // avoid bots that have the Invincible powerup
		if(autocvar_bot_ai_vore_relation > 0)
			priority *= pow(relationship, autocvar_bot_ai_vore_relation); // evaluate relationship
		if(IS_SWALLOWED(pred) && STAT(VORE_PROGRESS_PREY, pred) < 1 && pred.swallower == prey)
			priority *= 1 + STAT(VORE_PROGRESS_PREY, pred); // if this prey is trying to eat us, prioritize them
		if(skill >= 3) // SKILL 3 and up
		if(IS_PRED(pred) && Vore_HasTeamPrey(pred, pred, false))
			return 1; // we have team prey in our stomach, don't think about chasing enemies
		if(skill >= 5) // SKILL 5 and up
		if(prey.flagcarried || prey.ballcarried)
			priority *= 2; // prefer enemy carriers twice as much
		if(skill >= 7) // SKILL 7 and up
		if(Vore_HasTeamPrey(prey, pred, false))
			priority *= 1 + prey.stomach_fill; // if an enemy ate one of our team mates, we want to rescue them by attacking the pred
	}

	return priority;
}

// returns the best player that can be engaged within radius
entity Vore_Closest(entity pred, float range)
{
	float best_priority = 0;
	entity best_prey = NULL;
	entity e = NULL;
	for(e = findradius(pred.origin, range); e; e = e.chain)
	{
		if(IS_PLAYER(e))
		{
			float this_priority = Vore_Priority(pred, e);
			if(this_priority > best_priority)
			{
				best_priority = this_priority;
				best_prey = e;
			}
		}
	}
	return best_prey;
}

// the swallow AI
void Vore_AI_PredSwallow(entity pred)
{
	if(time < pred.decide_swallow)
		return;
	if(STAT(VORE_LOAD, pred) >= STAT(VORE_LOADMAX, pred))
		return;

	// time before the bot tries to swallow again
	pred.decide_swallow = max(time, pred.decide_swallow) + max(0.01, autocvar_bot_ai_vore_thinkinterval_swallow * (0.5 ** pred.bot_thinkpred) * (1 / skill));

	// determine our swallow range
	float swallow_range = autocvar_g_balance_vore_swallow_range;
	if(pred.scale) // we can swallow from further or closer based on our size
		swallow_range *= pow(pred.scale, autocvar_g_balance_vore_swallow_range_scalediff);

	// determine the player we should be aiming at
	entity prey_closest = Vore_Closest(pred, swallow_range);
	entity prey_havocbot = pred.enemy;
	if(autocvar_bot_ai_vore_aim_closest)
		pred.enemy_prey = IS_PLAYER(prey_closest) ? prey_closest : prey_havocbot; // prefer closest, fallback to havocbot
	else
		pred.enemy_prey = IS_PLAYER(prey_havocbot) ? prey_havocbot : prey_closest; // prefer havocbot, fallback to closest
	entity prey = IS_PLAYER(pred.enemy_prey) ? pred.enemy_prey : NULL;

	// check if we can swallow or feed the prey right now
	if(prey && !IS_PREY(prey) && !IS_DEAD(prey) && vdist(prey.origin - pred.origin, <=, swallow_range))
	{
		float scalediff = (prey.scale && pred.scale) ? prey.scale / pred.scale : 1;

		// decide if to swallow or feed ourselves based on bot preference as well as size difference
		if(!autocvar_bot_ai_vore_friendly && SAME_TEAM(pred, prey))
		{
			// if this is a team mate, take a pragmatic decision based on how much they or we need healing
			// default to acting as a predator, fallback to being a prey if that fails
			float difference = Vore_CheckDifference(pred, prey, autocvar_g_balance_vore_digestion_teamheal_health_stable, autocvar_g_balance_vore_digestion_teamheal_armor_stable);
			if(autocvar_g_vore_teamvore && autocvar_g_balance_vore_digestion_teamheal && difference > 1 - pred.bot_likepred && Vore_CheckHeal(prey))
			{
				// we must heal the prey
				pred.action_swallow = 1; // press the swallow key
				pred.action_feed = -1; // release the feed key
				Vore_SetWilling(pred, prey, false);
			}
			else if(autocvar_g_vore_teamvore && autocvar_g_balance_vore_digestion_teamheal && -difference > 1 - pred.bot_likeprey && Vore_CheckHeal(pred))
			{
				// the prey must heal us
				pred.action_swallow = -1; // release the swallow key
				pred.action_feed = 1; // press the feed key
				Vore_SetWilling(pred, prey, true);
			}
			else
			{
				// no healing available
				pred.action_swallow = -1; // release the swallow key
				pred.action_feed = -1; // release the feed key
			}
		}
		else
		{
			// factor in the health and armor difference between the predator and prey
			// we want to always engage enemies to prevent the game from stalling, force the decision to be either pred or prey
			float difference = Vore_CheckDifference(pred, prey, max(autocvar_g_balance_health_regenstable, autocvar_g_balance_health_rotstable), max(autocvar_g_balance_armor_regenstable, autocvar_g_balance_armor_rotstable));
			if(1 + difference + (pred.bot_likepred - pred.bot_likeprey) < min(scalediff, 2))
			{
				// we wish to be a prey
				pred.action_swallow = -1; // release the swallow key
				pred.action_feed = 1; // press the feed key
				Vore_SetWilling(pred, prey, true);
			}
			else
			{
				// we wish to be a pred
				pred.action_swallow = 1; // press the swallow key
				pred.action_feed = -1; // release the feed key
				Vore_SetWilling(pred, prey, false);
			}
		}

		// check if we can actually swallow or eat someone when attempting to swallow
		if(pred.action_feed > 0)
		{
			if(skill >= 3) // SKILL 3 and up
			if(!Vore_CanSwallow(prey, pred, true))
				pred.action_feed = -1; // release the feed key
			if(skill >= 7) // SKILL 7 and up
			if(!Vore_CanEat(prey, pred))
				pred.action_feed = -1; // release the feed key
		}
		if(pred.action_swallow > 0)
		{
			if(skill >= 3) // SKILL 3 and up
			if(!Vore_CanSwallow(pred, prey, false))
				pred.action_swallow = -1; // release the swallow key
			if(skill >= 7) // SKILL 7 and up
			if(!Vore_CanEat(pred, prey))
				pred.action_swallow = -1; // release the swallow key
		}
	}
	else
	{
		// release keys
		pred.action_swallow = -1; // release the swallow key
		pred.action_feed = -1; // release the feed key
	}
}

// the pred AI
void Vore_AI_Pred(entity pred)
{
	PHYS_INPUT_BUTTON_REGURGITATE(pred) = false; // this key must only be pressed once below, keep it released otherwise
	PHYS_INPUT_BUTTON_DIGEST(pred) = false; // this key must only be pressed once below, keep it released otherwise

	if(time < pred.decide_pred)
		return;
	if(!IS_PRED(pred))
		return;

	// time before the bot decides what to do with their prey
	pred.decide_pred = max(time, pred.decide_pred) + max(0.01, autocvar_bot_ai_vore_thinkinterval_pred * (0.5 ** pred.bot_thinkpred) * (1 / skill));

	entity prey = pred.enemy;

	// friendly and hostile targets are evaluated based on different principles
	if(Vore_HasTeamPrey(pred, pred, false))
	{
		// if friendly prey is present, decide whether or not to abandon them when seeing a foe we can attack
		// this causes the bot to try and regurgitate their team mate when seeing an enemy, hoping the enemy will still be there afterward
		if(skill >= 3) // SKILL 3 and up
		if(random() > autocvar_bot_ai_vore_goal_friendly)
		if(IS_PLAYER(prey) && !SAME_TEAM(pred, prey))
		if(Vore_CanSwallow(pred, prey, false))
		if(pred.health > autocvar_g_balance_vore_regurgitate_damage)
			PHYS_INPUT_BUTTON_REGURGITATE(pred) = true; // release team mates

		// if we're holding a team mate that's been healed to the maximum, we can release them
		// as regurgitating lets out a random player, only do this if the team mate is our only prey
		if(skill >= 5) // SKILL 5 and up
		if(autocvar_g_balance_vore_digestion_teamheal && !autocvar_bot_ai_vore_friendly)
		if(pred.health > autocvar_g_balance_vore_regurgitate_damage * autocvar_g_balance_vore_regurgitate_damage_team)
		{
			bool prey_regurgitate = false;
			int prey_count = 0;
			FOREACH_CLIENT(IS_PLAYER(it) && it != pred && it.predator == pred, {
				if(SAME_TEAM(it, pred) && !IS_DEAD(it) && !Vore_CheckHeal(it))
					prey_regurgitate = true;
				++prey_count;
			});
			if(prey_regurgitate && prey_count == 1)
				PHYS_INPUT_BUTTON_REGURGITATE(pred) = true; // release team mates
		}

		// start digesting only if we can team heal allies
		if(autocvar_g_balance_vore_digestion_teamheal && !pred.digesting)
			PHYS_INPUT_BUTTON_DIGEST(pred) = true;
	}
	else
	{
		// if enemy prey is present, decide whether or not to abandon them when seeing an ally we can heal
		// this causes the bot to try and regurgitate their enemy when seeing a team mate, hoping the team mate will still be there afterward
		if(skill >= 3) // SKILL 3 and up
		if(random() > autocvar_bot_ai_vore_goal_hostile)
		if(IS_PLAYER(prey) && SAME_TEAM(pred, prey) && Vore_CheckHeal(prey))
		if(Vore_CanSwallow(pred, prey, false))
		if(pred.health > autocvar_g_balance_vore_regurgitate_damage)
			PHYS_INPUT_BUTTON_REGURGITATE(pred) = true; // release enemy

		// if the predator has reached the maximum amount of health / armor they may gain from digestion, there's no benefit to keeping dead prey any longer
		if(skill >= 5) // SKILL 5 and up
		if(IS_PRED(pred) && !Vore_HasLivePrey(pred, false))
		if(pred.health > autocvar_g_balance_vore_regurgitate_damage)
		if(pred.health >= autocvar_g_balance_vore_digestion_vampire_health_stable && pred.armorvalue >= autocvar_g_balance_vore_digestion_vampire_armor_stable)
			PHYS_INPUT_BUTTON_REGURGITATE(pred) = true;

		// if the predator's health is less than the damage of the stomach kicks their prey can deal, try to regurgitate some players
		if(skill >= 7) // SKILL 7 and up
		if(pred.health > autocvar_g_balance_vore_regurgitate_damage && Vore_CheckOverload(pred))
			PHYS_INPUT_BUTTON_REGURGITATE(pred) = true;

		// start digesting
		if(!pred.digesting)
			PHYS_INPUT_BUTTON_DIGEST(pred) = true;
	}
}

// the prey AI
void Vore_AI_Prey(entity prey)
{
	entity pred = prey.predator;
	float scalediff = (prey.scale && pred.scale) ? prey.scale / pred.scale : 1;

	if((IS_PREY(prey) || IS_SWALLOWED(prey)) && (SAME_TEAM(prey, pred) || Vore_IsWilling(prey, pred)))
	{
		// if we're eaten by a team mate or willing prey, never kick or move
		PHYS_INPUT_MOVEVALUES(prey) = '0 0 0';
		PHYS_INPUT_BUTTON_JUMP(prey) = false;
		PHYS_INPUT_BUTTON_CROUCH(prey) = false;
	}
	else if(IS_SWALLOWED(prey) && STAT(VORE_PROGRESS_PREY, prey) * 10 >= 10 - skill)
	{
		// while being swallowed by enemies, smart bots will jump to make it harder to get caught
		PHYS_INPUT_BUTTON_JUMP(prey) = true;
	}

	if(time < prey.decide_prey)
		return;
	if(!IS_PREY(prey))
		return;

	// time before the bot decides what to do with their predator
	prey.decide_prey = max(time, prey.decide_prey) + max(0.01, autocvar_bot_ai_vore_thinkinterval_prey * (0.5 ** pred.bot_thinkprey) * (1 / skill));

	// probabilistically decide if to attack or not based on prey preference, choosing a random kick direction if yes
	if(!SAME_TEAM(prey, pred) && !Vore_IsWilling(prey, pred) && random() > prey.bot_likeprey)
		PHYS_INPUT_MOVEVALUES(prey) = '1 1 1' - (randomvec() * 2);

	// if a bot can willingly leave the predator, do so unless there's a reason not to
	if(Vore_CanLeave(prey))
	{
		bool heal = autocvar_g_balance_vore_digestion_teamheal && SAME_TEAM(prey, pred);

		if(skill >= 3) // SKILL 3 and up
		if(pred.digesting && !heal)
			PHYS_INPUT_BUTTON_USE(prey) = true; // our predator is digesting, get out immediately
		if(skill >= 5) // SKILL 5 and up
		if(!autocvar_bot_ai_vore_friendly)
		if((!pred.digesting && heal) || !Vore_CheckHeal(prey))
			PHYS_INPUT_BUTTON_USE(prey) = true; // leave if we aren't being team healed
	}

	// change willingness based on current circumstances
	// players can only do this when prey firing their weapons is disabled, so apply this limitation to bots as well
	if(!autocvar_g_balance_vore_weapons_fire)
	{
		float difference = Vore_CheckDifference(pred, prey, max(autocvar_g_balance_health_regenstable, autocvar_g_balance_health_rotstable), max(autocvar_g_balance_armor_regenstable, autocvar_g_balance_armor_rotstable));
		if(1 + difference + (prey.bot_likepred - prey.bot_likeprey) < min(scalediff, 2) || SAME_TEAM(prey, pred) || Vore_IsWilling(prey, pred))
			Vore_SetWilling(pred, prey, true);
		else
			Vore_SetWilling(pred, prey, false);
	}
}

// --------------------------------
// Havocbot functions:
// --------------------------------

void havocbot_goalrating_eatplayers(entity this, float ratingscale, vector org, float sradius)
{
	FOREACH_CLIENT(IS_PLAYER(it) && it != this, {
		// skip if the distance is beyond what the bot can see
		if(vdist(it.origin - org, >, sradius))
			continue;

		// set the priority of this target
		// this.enemy becomes the prey when successful
		// a priority of 0 means we wish to ignore the player
		// a priority of 1 means no change
		// the priority is added to half of the ratingscale, in order to ensure that bots never ignore valid targets
		float rating = 0;
		float priority = Vore_Priority(this, it);
		if(priority > 0)
			rating = (ratingscale / 2) * (1 + priority);

		navigation_routerating(this, it, rating, 5000);
	});
}

void havocbot_role_eatplayers(entity this)
{
	if(IS_DEAD(this) || IS_PREY(this))
		return;

	if(navigation_goalrating_timeout(this))
	{
		// an equal priority between items, attacks, and voring seems to provide the best results
		navigation_goalrating_start(this);
		havocbot_goalrating_items(this, 10000, this.origin, 10000);
		havocbot_goalrating_enemyplayers(this, 10000, this.origin, 10000);
		havocbot_goalrating_eatplayers(this, 10000, this.origin, 10000);
		havocbot_goalrating_waypoints(this, 1, this.origin, 3000);
		navigation_goalrating_end(this);

		navigation_goalrating_timeout_set(this);
	}
}

// --------------------------------
// Hook functions:
// --------------------------------

// GetPressedKeys mutator hook
void VoreBot_GetPressedKeys(entity this)
{
	// only execute for bots
	if(clienttype(this) != CLIENTTYPE_BOT || IS_DEAD(this) || autocvar_bot_novore || !skill)
	{
		this.action_swallow = 0;
		this.action_feed = 0;
		return;
	}

	// --------------------------------
	// Code that addresses both prey and predators:
	// --------------------------------

	// apply forced keys
	if(this.action_swallow)
		PHYS_INPUT_BUTTON_ATCK(this) = this.action_swallow > 0;
	if(this.action_feed)
		PHYS_INPUT_BUTTON_ATCK2(this) = this.action_feed > 0;

	// update player relationships
	if(autocvar_bot_ai_vore_relation > 0)
	{
		float interest = (this.bot_likepred + this.bot_likeprey) / 2;
		FOREACH_CLIENT(IS_PLAYER(it) && it != this && vdist(it.origin - this.origin, <=, 10000), {
			int it_entnum = num_for_edict(it);
			float relation_old = this.relation[it_entnum];

			// increase the relation if the target is a prey, decrease it if the target is a pred
			if(IS_PREY(it))
			{
				if(IS_PRED(this) && it.predator == this)
				{
					if(Vore_IsWilling(it, this))
						this.relation[it_entnum] += frametime * interest * autocvar_bot_ai_vore_relation_add_self * autocvar_bot_ai_vore_relation_add_self_willing;
					else
						this.relation[it_entnum] += frametime * interest * autocvar_bot_ai_vore_relation_add_self;
				}
				else
				{
					this.relation[it_entnum] += frametime * interest * autocvar_bot_ai_vore_relation_add_other;
				}
			}
			if(IS_PRED(it))
			{
				if(IS_PREY(this) && this.predator == it)
				{
					if(Vore_IsWilling(this, it))
						this.relation[it_entnum] -= frametime * interest * autocvar_bot_ai_vore_relation_add_self * autocvar_bot_ai_vore_relation_add_self_willing;
					else
						this.relation[it_entnum] -= frametime * interest * autocvar_bot_ai_vore_relation_add_self;
				}
				else
				{
					this.relation[it_entnum] -= frametime * interest * autocvar_bot_ai_vore_relation_add_other;
				}
			}
			this.relation[it_entnum] = bound(-1, this.relation[it_entnum], 1);

			// notify the player when a bot has switched to given relationship toward them
			if(clienttype(it) == CLIENTTYPE_REAL)
			{
				if(autocvar_bot_ai_vore_relation_notify_prey > 0)
				{
					if(relation_old < autocvar_bot_ai_vore_relation_notify_prey && this.relation[it_entnum] >= autocvar_bot_ai_vore_relation_notify_prey)
					{
						Send_Notification(NOTIF_ONE_ONLY, it, MSG_CENTER, CENTER_VORE_RELATION_PREY_START, this.netname);
						play2(it, SND(KH_ALARM));
					}
					else if(relation_old > autocvar_bot_ai_vore_relation_notify_prey && this.relation[it_entnum] <= autocvar_bot_ai_vore_relation_notify_prey)
					{
						Send_Notification(NOTIF_ONE_ONLY, it, MSG_CENTER, CENTER_VORE_RELATION_PREY_END, this.netname);
						play2(it, SND(KH_ALARM));
					}
				}
				if(autocvar_bot_ai_vore_relation_notify_pred > 0)
				{
					if(relation_old > -autocvar_bot_ai_vore_relation_notify_pred && this.relation[it_entnum] <= -autocvar_bot_ai_vore_relation_notify_pred)
					{
						Send_Notification(NOTIF_ONE_ONLY, it, MSG_CENTER, CENTER_VORE_RELATION_PRED_START, this.netname);
						play2(it, SND(KH_ALARM));
					}
					else if(relation_old < -autocvar_bot_ai_vore_relation_notify_pred && this.relation[it_entnum] >= -autocvar_bot_ai_vore_relation_notify_pred)
					{
						Send_Notification(NOTIF_ONE_ONLY, it, MSG_CENTER, CENTER_VORE_RELATION_PRED_END, this.netname);
						play2(it, SND(KH_ALARM));
					}
				}
			}
		});
	}

	// --------------------------------
	// Code that addresses predators:
	// --------------------------------

	if(!IS_PREY(this))
	{
		// run the swallow and pred AI
		Vore_AI_PredSwallow(this);
		Vore_AI_Pred(this);
	}

	// --------------------------------
	// Code that addresses prey:
	// --------------------------------

	// run the prey AI
	Vore_AI_Prey(this);
}

// ClientConnect mutator hook
void VoreBot_ClientConnect(entity this)
{
	// clear bot relations
	int this_entnum = num_for_edict(this);
	FOREACH_CLIENT(IS_PLAYER(it) && clienttype(it) == CLIENTTYPE_BOT, {
		it.relation[this_entnum] = 0;
	});
}

// BotShouldAttack mutator hook, returns bool "attack"
bool VoreBot_BotShouldAttack_Attack(entity this, entity player_target)
{
	if(this.action_swallow > 0 || this.action_feed > 0)
		return true; // if we're swallowing or feeding, we aren't conventionally attacking our opponent

	if(IS_PREY(this) || IS_PREY(player_target) || SAME_TEAM(this, player_target) || Vore_IsWilling(this, player_target))
		return true; // don't attack if either us or the target are prey or team mates, or we are willing toward them

	if(IS_SWALLOWED(player_target) && player_target.swallower == this && Vore_IsWilling(player_target, this))
		return true; // don't attack a willing prey feeding themselves to us

	// fight back if the player in cause is eating us
	if(skill >= 3) // SKILL 3 and up
	if(IS_SWALLOWED(this) && this.swallower == player_target)
		return false;

	// shooting the player we're eating helps if damage increases their swallow progress
	// at the same time, the force might push them away and ruin our attempts to eat them
	// as such, shoot on early progress but abstain if the player is almost eaten
	if(skill >= 5) // SKILL 5 and up
	if(IS_SWALLOWED(player_target) && player_target.swallower == this)
		return (STAT(VORE_PROGRESS_PREY, player_target) <= random());

	// don't waste ammo on players that can't be harmed
	if(skill >= 7) // SKILL 7 and up
	if(player_target.health <= autocvar_g_balance_vore_minweapondamage_health && player_target.armorvalue <= autocvar_g_balance_vore_minweapondamage_armor)
		return true;

	return false;
}

// HavocBot_Aim mutator hook, returns bool "aim"
bool VoreBot_HavocBot_Aim(entity this)
{
	entity prey = this.enemy_prey;

	// determine our swallow range
	float swallow_range = autocvar_g_balance_vore_swallow_range;
	if(this.scale) // we can swallow from further or closer based on our size
		swallow_range *= pow(this.scale, autocvar_g_balance_vore_swallow_range_scalediff);

	// make sure the bot is always aiming toward the player they're trying to swallow or feed
	if(skill > 0)
	if(IS_PLAYER(prey) && vdist(prey.origin - this.origin, <=, swallow_range))
	if((this.action_swallow && Vore_CanSwallow(this, prey, false)) || (this.action_feed && Vore_CanSwallow(this, prey, true)))
	{
		// aiming method 1
		// only works at certain times and is highly complex
		vector this_vel = (!this.waterlevel) ? vec2(this.velocity) : this.velocity;
		vector prey_vel = (!prey.waterlevel) ? vec2(prey.velocity) : prey.velocity;
		lag_additem(this, time + CS(this).ping, 0, 0, prey, this.origin, this_vel, (prey.absmin + prey.absmax) * 0.5, prey_vel);

		// aiming method 2
		// does not aim reliably and causes division by zero warnings
		/*
			float deviation = 1 / skill / this.bot_aimskill;
			bot_aimdir(this, prey.origin + prey.view_ofs - this.origin - this.view_ofs, deviation);
		*/

		return true;
	}
	return false;
}

// HavocBot_ChooseRole mutator hook
void VoreBot_HavocBot_ChooseRole(entity this)
{
	// override the generic bot role with the vore role
	// this is itself overridden by gametype specific roles
	this.havocbot_role = havocbot_role_eatplayers;
}
