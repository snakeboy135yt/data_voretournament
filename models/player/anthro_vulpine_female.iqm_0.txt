name Anthro Vulpine Female
species human
sex Female
weight 105
age 26
description A warrior of the anthro race
bone_upperbody spine2
bone_aim0 0.25 spine2
bone_aim1 0.4 spine4
bone_aim2 0.2 upperarm_L
bone_aim3 0.35 bip01 r hand
bone_weapon bip01 r hand
fixbone 1
